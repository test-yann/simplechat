FROM node:17-alpine
RUN mkdir -p /home/node/app/node_modules
WORKDIR /home/node/app
COPY ./package*.json ./
RUN npm install && apk add --no-cache openssl
COPY ./ ./
EXPOSE 80
CMD [ "node", "./server.js" ]